<?php

/**
 * Implements hook_permission().
 */
function print_permission() {
  $permission['access print pages'] = array(
    'title' => t('Access print-friendly version of pages.'),
  );
  $permission['administer print'] = array(
    'title' => t('Administer print settings'),
  );

  return $permission;
}

/**
 * Implements hook_init().
 */
function print_init() {
  if (user_access('access print pages')) {
    if (isset($_GET['render']) && $_GET['render'] == 'print') {
      print_is_print_page(TRUE);
      unset($_GET['render']);
    }
  }
}

function print_menu() {
  $items = array();

  $entity_info = entity_get_info();
  foreach (array_keys($entity_info) as $entity_type) {
    if (!empty($entity_info[$entity_type]['default path'])) {
      $entity_path = $entity_info[$entity_type]['default path'];
      // Determine which argument is the entity ID.
      $args = explode('/', $entity_path);
      $arg_index = array_search('%' . $entity_type, $args);

      $items[$entity_path . '/print'] = array(
        'page callback' => 'print_entity_print_page',
        'page arguments' => array($entity_type, $arg_index),
        'access callback' => 'print_entity_print_access',
        'access arguments' => array($entity_type, $arg_index),
        'weight' => 100,
        'type' => MENU_CALLBACK,
      );
    }
  }
}

function print_entity_print_page($entity_type, $entity_id) {
  $entities = entity_load($entity_type, array($entity_id));
  return entity_view($entity_type, $entities, 'print');
}

function print_entity_print_access($entity_type, $entity_id, $account = NULL) {
  if (!user_access('access print pages', $account)) {
    return FALSE;
  }

  $entity = entity_load_single($entity_type, $entity_id);
  return !empty($entity) && entity_access('view', $entity_type, $entity, $account);
}

/**
 * Implements hook_entity_info_alter().
 */
function print_entity_info_alter(&$info) {
  $defaults['node']['default path'] = 'node/%node';
  $defaults['comment']['default path'] = 'comment/%comment';
  $defaults['taxonomy_term']['default path'] = 'taxonomy/term/%taxonomy_term';
  $defaults['user']['default path'] = 'user/%user';

  $default_paths = array(
    'node' => 'node/%node',
    'comment' => 'comment/%commment',
    'user' => 'user/%user',
    'taxonomy_term' => 'taxonomy/term/%taxonomy_term',
  );

  foreach ($default_paths as $entity_type => $default_path) {
    if (isset($info[$entity_type]) && !isset($info[$entity_type]['default path'])) {
      $info[$entity_type]['default path'] = $default_path;

      // Add the 'print' view mode.
      if (!isset($info[$entity_type]['view modes']['print'])) {
        $info[$entity_type]['view modes']['print'] = array(
          'label' => t('Print'),
          'custom settings' => FALSE,
        );
      }
    }
  }
}

function print_is_print_page($mode = NULL) {
  global $base_path;
  $is_print_page = &drupal_static(__FUNCTION__, NULL);

  // Make sure external resources are not included more than once. Also return
  // the current mode, if no mode was specified.
  if (isset($is_print_page) || !isset($mode)) {
    return (bool) $is_print_page;
  }

  $is_print_page = $mode;

  if ($is_print_page) {
    // Allow modules to act upon print page events.
    module_invoke_all('print_page_initialize');
  }

  return (bool) $is_print_page;
}

function print_page_alter(&$page) {
  // If we are limiting rendering to a subset of page regions, deny access to
  // all other regions so that they will not be processed.
  if ($regions_to_render = print_get_regions_to_render()) {
    $skipped_regions = array_diff(element_children($page), $regions_to_render);
    foreach ($skipped_regions as $skipped_region) {
      $page[$skipped_region]['#access'] = FALSE;
    }
  }

  if (print_is_print_page()) {
    admin_menu_suppress(TRUE);
    //array_unshift($page['#theme_wrappers'], 'print');
  }
}

function _print_region_list($type) {
  // Obtain the current theme. We need to first make sure the theme system is
  // initialized, since this function can be called early in the page request.
  drupal_theme_initialize();
  $themes = list_themes();
  $theme = $themes[$GLOBALS['theme']];
  // Return the list of regions stored within the theme's info array, or an
  // empty array if no regions of the appropriate type are defined.
  return !empty($theme->info[$type]) ? $theme->info[$type] : array();
}

function print_regions() {
  return _print_region_list('print_regions');
}

function print_set_regions_to_render($regions = NULL) {
  $regions_to_render = &drupal_static(__FUNCTION__, array());
  if (isset($regions)) {
    $regions_to_render = $regions;
  }
  return $regions_to_render;
}

function print_get_regions_to_render() {
  return print_set_regions_to_render();
}

function print_block_list_alter(&$blocks) {
  // If we are limiting rendering to a subset of page regions, hide all blocks
  // which appear in regions not on that list. Note that print_page_alter()
  // does a more comprehensive job of preventing unwanted regions from being
  // displayed (regardless of whether they contain blocks or not), but the
  // reason for duplicating effort here is performance; we do not even want
  // these blocks to be built if they are not going to be displayed.
  if ($regions_to_render = print_get_regions_to_render()) {
    foreach ($blocks as $bid => $block) {
      if (!in_array($block->region, $regions_to_render)) {
        unset($blocks[$bid]);
      }
    }
  }
}

/**
 * Implements hook_system_info_alter().
 *
 * Add default regions for the print pages.
 */
function print_system_info_alter(&$info, $file, $type) {
  if ($type == 'theme') {
    $info['print_regions'][] = 'content';
    $info['print_regions'][] = 'help';
  }
}

function print_preprocess_page(&$variables) {
  if (print_is_print_page()) {
    unset($variables['tabs']['#primary']);
  }
}

function print_print_page_initialize() {
  print_set_regions_to_render(print_regions());
}

function print_preprocess_node(&$variables) {
  if (print_is_print_page()) {
    if (isset($variables['content']['comments']['comment_form'])) {
      // To hide the comment form and the 'Add new comment' title we have to
      // set this to a blank array.
      $variables['content']['comments']['comment_form'] = array();
    }
    if (isset($variables['content']['links'])) {
      $variables['content']['links']['#access'] = FALSE;
    }
  }
}

/**
 * Implements hook_entity_view().
 */
function print_entity_view($entity, $entity_type, $view_mode, $langcode) {
  if (!print_is_print_page()) {
    $uri = entity_uri($entity_type, $entity);
    // Add a link to the printer-friendly version.
    $links['print'] = array(
      'title' => t('Print'),
      'href' => $uri['path'] . '/print',
      'query' => array('render' => 'print'),
      'attributes' => array(
        'title' => t('Show a printer-friendly version of this page.'),
      ),
      // @todo By using current_path() this link gets marked with an active
      // class, so we should probably remove that styling via CSS/JS.
    );
    if (!isset($entity->content['links'])) {
      $entity->content['links'] = array(
        '#theme' => "links__{$entity_type}",
        '#pre_render' => array('drupal_pre_render_links'),
        '#attributes' => array('class' => array('links', 'inline')),
      );
    }
    $entity->content['links']['print'] = array(
      '#theme' => "links__{$entity_type}__print",
      '#links' => $links,
      '#attributes' => array('class' => array('link', 'inline')),
    );
  }
}
